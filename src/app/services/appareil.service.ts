import { Appareil } from './../models/appareil';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppareilService {

  appareilName: any;

// appareil = {
//     name: '',
//     description: []
// };

  appareilsList: Appareil[] = [
    {
    name: 'Télévision',
    description: [
      'dimension : 40 pouces ',
      'consommation : 22 KWh/an'
           ],
    isOn: false
  },
  {
    name: 'Ordinateur',
    description: [
      'marque : Apple',
      'type : MacBook Pro',
      'consommation : 500 KWh/an'
        ],
        isOn: true
  },
  {
    name: 'Machine à laver',
    description: [
      'marque : Vedette',
      'capacité : 6 litre',
      'consommation : 173 KWh/an'
        ],
        isOn: false
  }
];
  indexSelected: number;

  constructor() { }
}
