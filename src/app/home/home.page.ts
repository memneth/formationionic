import { AppareilsPage } from './../appareils/appareils.page';
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private navController: NavController ) {}

  onGoToAppareils() {
    this.navController.navigateRoot('menu/tabs');
  }
  // onGoToSettings() {
  //   this.navController.navigateRoot('/settings');
  // }
  // onGoToTabs() {
  //   this.navController.navigateRoot('/tabs');
  // }
}
