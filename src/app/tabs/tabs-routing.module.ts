import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'appareils',
        loadChildren: () => import('../appareils/appareils.module').then( m => m.AppareilsPageModule)
      },
      {
        path: 'appareils/singleAppareils',
        loadChildren: () => import('../appareils/single-appareils/single-appareils.module').then( m => m.SingleAppareilsPageModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('../settings/settings.module').then( m => m.SettingsPageModule)
      },
  ]
},
{
  path: '',
  redirectTo: 'tabs/appareils',
  pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
