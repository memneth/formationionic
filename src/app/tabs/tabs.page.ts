import { SettingsPage } from './../settings/settings.page';
import { AppareilsPage } from './../appareils/appareils.page';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  // initialisation des variables pour la gestion des tabs
  appareilsPage = AppareilsPage;
  settingsPage = SettingsPage;

  

  constructor() { }

  ngOnInit() {
  }

}
