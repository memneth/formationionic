import { ModalExamplePage } from './../modal-example/modal-example.page';
import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  constructor(private modalController: ModalController, private alertController: AlertController) { }

  ngOnInit() {
  }

  async onAlerte() {
    const alert = await this.alertController.create({
     header: 'Confirmation allumage',
     subHeader: 'Confirmer vous l\'allumage de toutes les lampes',
     buttons:[
       {
         text: 'Annuler',
         role: 'cancel',
         handler: () => {
           console.log(' clic sur annuler ');
       }
       },
       {
         text: 'Confirmer',
         handler: () => {
           console.log('clic sur confirmer');
         }
       }
     ]
   });

    await alert.present();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalExamplePage,
      componentProps: {
        'firstName': 'Douglas',
        'lastName': 'Adams',
        'middleInitial': 'N'
      }
    });
   await modal.present();
  }

}
