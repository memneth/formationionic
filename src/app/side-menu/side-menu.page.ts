import { Router, RouterEvent } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.page.html',
  styleUrls: ['./side-menu.page.scss'],
})
export class SideMenuPage implements OnInit {

  pages = [
    {
      title: 'Accueil',
      url: '/menu/home'
    },
    {
      title: 'Contacts',
      url: '/menu/contacts'
    },
  {
    title: 'Options',
    url: '/menu/options'
  },
];
  selectedPath = '';
/**
 * Déclenchement d'un évent lors de la selection d'un item de menu
 * pour colorer la bordure gauche du menu
 * Le selectedPath sera utilisé par le css (class.active-item sur page html)
 * @param router 
 */
  constructor(private router: Router) { 
    router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
  }

  ngOnInit() {
  }

}
