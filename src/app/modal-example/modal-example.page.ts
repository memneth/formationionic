import { Component, OnInit, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-example',
  templateUrl: './modal-example.page.html',
  styleUrls: ['./modal-example.page.scss'],
})
export class ModalExamplePage implements OnInit {

   // Data passed in by componentProps
   @Input() firstName: string;
   @Input() lastName: string;
   @Input() middleInitial: string;
 
   constructor(private modalCtrl: ModalController, navParams: NavParams) {
     // componentProps can also be accessed at construction time using NavParams
     console.log(navParams.get('firstName'));
   }
 


  ngOnInit() {
  }

  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
