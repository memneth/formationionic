import { NavParams } from '@ionic/angular';
import { Appareil } from './../../models/appareil';
import { AppareilService } from './../../services/appareil.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-appareils',
  templateUrl: './single-appareils.page.html',
  styleUrls: ['./single-appareils.page.scss'],
})
export class SingleAppareilsPage implements OnInit {

  appareilName: any;

  appareil: Appareil;
  index: number;

  //1ère solution avec Query Param
  // constructor(private route: ActivatedRoute, private router: Router) {
  //   this.route.queryParams.subscribe(params => {
  //     if (params && params.special) {
  //       this.appareilName = JSON.parse(params.special);
  //     }
  //   });
  // }

  //2 ème solution avec service
  constructor(private appareilService: AppareilService) {
    //this.appareilName = appareilService.appareilName;
    //this.appareil = appareilService.appareil;
    console.log('recupération index  appareilService: ', appareilService.indexSelected);
    this.index = appareilService.indexSelected;
    this.appareil = appareilService.appareilsList[this.index];
  }

  ngOnInit() {
  }

  onToggle() {
    this.appareil.isOn = ! this.appareil.isOn;
  }

}
