import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SingleAppareilsPageRoutingModule } from './single-appareils-routing.module';

import { SingleAppareilsPage } from './single-appareils.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SingleAppareilsPageRoutingModule
  ],
  declarations: [SingleAppareilsPage]
})
export class SingleAppareilsPageModule {}
