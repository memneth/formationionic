import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingleAppareilsPage } from './single-appareils.page';

const routes: Routes = [
  {
    path: '',
    component: SingleAppareilsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SingleAppareilsPageRoutingModule {}
