import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppareilsPage } from './appareils.page';

const routes: Routes = [
  {
    path: '',
    component: AppareilsPage
  },
  {
    path: 'single-appareils',
    loadChildren: () => import('./single-appareils/single-appareils.module').then( m => m.SingleAppareilsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppareilsPageRoutingModule {}
