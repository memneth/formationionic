import { Appareil } from './../models/appareil';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareils',
  templateUrl: './appareils.page.html',
  styleUrls: ['./appareils.page.scss'],
})
export class AppareilsPage implements OnInit {

  appareilsList: Appareil[];

  constructor(private appareilService: AppareilService, private router: Router) { }

  ngOnInit() {
   // this.appareilsList = this.appareilService.appareilsList;
    console.log('appareilsPage on init :', this.appareilsList);
  }

//   onLoadAppareils(name: string) {
//     // 1 ère solution query param (pas propre)
//     // const navigationExtras: NavigationExtras = {
//     //   queryParams: {
//     //     special: JSON.stringify(name)
//     //   }
//     // };
//     // this.router.navigate(['/appareils/singleAppareils'], navigationExtras);

//     // 2 ème solution utilisation d'un service
// this.appareilService.appareilName = name;
// this.router.navigate(['/tabs/appareils/singleAppareils']);
//   }
//permet de rafraichir l'objet liste (traitement des modal, etc.)
ionViewWillEnter(){
  this.appareilsList = this.appareilService.appareilsList.slice();
}

  // onLoadAppareil(appareil: {name: string, description: string[]}) {
  //   console.log('onLoadAppareil name', appareil.name);
  //   this.appareilService.appareil = appareil;
  //   this.router.navigate(['/tabs/appareils/singleAppareils']);
  // }

  // utilisation de l'index et de la navigation extra
  onLoadAppareil( pIndex: number) {
    console.log('onLoadAppareil index :', pIndex);
    this.appareilService.indexSelected = pIndex;
    this.router.navigate(['menu/tabs/tabs/appareils/singleAppareils']);
  }

}
