import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

// const routes: Routes = [
//   { path: '', redirectTo: 'tabs', pathMatch: 'full' },
//   { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
//   {
//     path: 'appareils',
//     loadChildren: () => import('./appareils/appareils.module').then( m => m.AppareilsPageModule)
//   },
//   {
//     path: 'appareils/singleAppareils',
//     loadChildren: () => import('./appareils/single-appareils/single-appareils.module').then( m => m.SingleAppareilsPageModule)
//   },
//   {
//     path: 'settings',
//     loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
//   },
//   {
//     path: 'tabs',
//     loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule)
//   },
// ];

// intégration de la navigation  via les tabs
const routes: Routes = [
  { 
    path: '', 
    redirectTo: '/menu/home',
    pathMatch: 'full'
  },
  {
    path: 'menu/home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./side-menu/side-menu.module').then( m => m.SideMenuPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
