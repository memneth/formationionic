export class Appareil {
   // name: string;
    description: string[];
    isOn: boolean;
// Propriété TypeScript : le fait de mettre "public name"  dans le constructeur implique 
// la déclaration de la variable  (L2) et l'affectation de la donnée (L8)
    constructor(public name: string) {
     //     this.name = name;
          this.isOn = false;
    }
}
